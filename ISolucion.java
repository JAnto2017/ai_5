package AI_5;

/**
 * Representa una solución potencial para un problema determinado.
 * Tiene una propiedad que permite conocer su valor.
 */
public interface ISolucion {
    double getValor();
}
