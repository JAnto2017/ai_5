package AI_5;

/**
 * El código propuesto está disponible para distintos tipos de aplicación sin necesidad de modificación.
 * Aplicación en modo consola.
 */
public interface IHM {
    void MostrarMensaje(String msg);
}
