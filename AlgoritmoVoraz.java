package AI_5;

public abstract class AlgoritmoVoraz extends Algoritmo {
    @Override
    public void Resolver(IProblema _pb, IHM _ihm) {
        
        super.Resolver(_pb, _ihm);
        ConstruirSolucion();
        DevolverResultado();
    }

    protected abstract void ConstruirSolucion();
}
