package AI_5;

import java.util.ArrayList;

/**
 * Se parte del descenso por gradiente.
 * Guardamos la mejor solución encontrada.
 * Podemos aceptar una solución menos óptima.
 * Empieza inicializando temperatura hasta alcanzar criterio de parada.
 * En cada iteración, se recupera el vecindario de la solución en curso y en particular la mejor solución en su interior. A continuación se actualiza o no, esta última.
 * El bucle termina incrementando las variables internas y modificando la temperatura.
 * El algoritmo termina con la visualización de los resultados.
 */
public abstract class RecocidoSimulado extends Algoritmo {
    protected ISolucion solucionActual;
    protected ISolucion mejorSolucion;
    protected double temperatura;

    @Override
    public void Resolver(IProblema _pb, IHM _ihm) {
        
        super.Resolver(_pb, _ihm);

        solucionActual = problema.SolucionAleatoria();
        mejorSolucion = solucionActual;
        InicializarTemperatura();

        while (!CriterioParada()) {
            ArrayList<ISolucion> vecindario = problema.Vecindario(solucionActual);

            if (vecindario != null) {
                ISolucion mejorSolucion = problema.MejorSolucion(vecindario);
                Actualizar(mejorSolucion);
            }

            Incrementar();
            ActualizarTemperatura();
        }
        DevolverResultado();
    }

    protected abstract void ActualizarTemperatura();
    protected abstract void InicializarTemperatura();
    protected abstract boolean CriterioParada();
    protected abstract void Actualizar(ISolucion solucion);
    protected abstract void Incrementar();
}
