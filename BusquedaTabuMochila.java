package AI_5;

import java.util.ArrayList;

public class BusquedaTabuMochila extends BusquedaTabu {
    protected int numIteracionesSinMejora = 0;
    protected int numIteraciones = 0;
    protected ArrayList<SolucionMochila> listaTabu = new ArrayList<>();

    private final static int NUM_MAX_ITERACIONES = 100;
    private final static int NUM_MAX_ITERACIONES_SIN_MEJORA = 30;
    private final static int NUM_MAX_POSICIONES_TABUS = 50;

    /**
     * Indica si se ha alcanzado el criterio de parada.
     * Verifica si se ha superado alguno de nuestros dos valores máx de iteraciones.
     * 
     */
    @Override
    protected boolean CriterioParada() {        
        return (numIteraciones > NUM_MAX_ITERACIONES || numIteracionesSinMejora > NUM_MAX_ITERACIONES_SIN_MEJORA);
    }

    /**
     * Actualiza la solución actual por la solución que se pasa como parámetro. 
     * Verificamos si la mejor solución propuesta está contenida en la lista de posiciones tabú.
     * Si se alcanza una solución mejor que la obtenida hasta el momento, se actualiza la variable mejorSolucion.
     * Ponemos a 0 
     */
    @Override
    protected void Actualizar(ISolucion solucion) {
        if (!listaTabu.contains(solucion)) {
            solucionActual = solucion;
            AgregarListaTabu(solucion);
            if (mejorSolucion.getValor() < solucion.getValor()) {
                mejorSolucion = solucion;
                numIteracionesSinMejora = 0;
            }
        }
    }

    /**
     * Permite actualizar las variables internas.
     * Incrementamos las dos variables correspondientes al nº de iteraciones.
     * 
     */
    @Override
    protected void Incrementar() {
        numIteracionesSinMejora++;
        numIteraciones++;
    }

    /**
     * El cuarto es el que permite mostrar la mejor solución.
     */
    @Override
    protected void DevolverResultado() {
        ihm.MostrarMensaje(mejorSolucion.toString());
    }

    /**
     * Agrega la posición indicada a la lista de posiciones tabú.
     * Se verifica en primer lugar si se ha alcanzado el nº máx. de posiciones y en caso afirmativo se elimina la primera de la lista antes de agregar la nueva posición.
     * 
     */
    @Override
    protected void AgregarListaTabu(ISolucion solucion) {
        while (listaTabu.size() >= NUM_MAX_POSICIONES_TABUS) {
            listaTabu.remove(0);
        }
        listaTabu.add((SolucionMochila)solucion);
    }

    /**
     * Devuelve las posiciones de un vecindario que no están en la lista tabú. Para ello, se utiliza el método removeAll.
     * 
     */
    @Override
    protected ArrayList<ISolucion> EliminarSolucionTabus(ArrayList<ISolucion> vecindario) {
        vecindario.removeAll(listaTabu);
        return vecindario;
    }
}
