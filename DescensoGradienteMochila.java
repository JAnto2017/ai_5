package AI_5;

public class DescensoGradienteMochila extends DescensoGradiente {
    protected int numIteracionesSinMejora = 0;
    protected final static int NUM_MAX_ITERACIONES_SIN_MEJORA = 50;

    /**
     * Debe indicar cuándo se ha alcanzado el criterio de parada.
     * Basta con verificar si se ha alcanzado el límite del número de iteraciones sin mejora.
     * 
     */
    @Override
    protected boolean CriterioParada() {        
        return numIteracionesSinMejora >= NUM_MAX_ITERACIONES_SIN_MEJORA;
    }

    /**
     * Debe incrementar las distintas variables internas.
     * Tenemos que incrementar únicamente el número de iteraciones sin mejora.
     * 
     */
    @Override
    protected void Incrementar() {
        numIteracionesSinMejora++;
    }

    /**
     * Actualiza una solución reemplazándola por la que se pasa como parámetro.
     * Se mira si el valor es mejor que el de la solución en curso. En caso afirmativo, se reemplaza esta y se pone a 0 el contador que indica el número de iteraciones sin mejora.
     * 
     */
    @Override
    protected void Actualizar(ISolucion solucion) {
        if (solucion.getValor() > solucionActual.getValor()) {
            solucionActual = solucion;
            numIteracionesSinMejora = 0;            
        }
    }

    /**
     * Devuelve el resultado, solución en curso.
     */
    @Override
    protected void DevolverResultado() {
        ihm.MostrarMensaje(solucionActual.toString());
    }
}
