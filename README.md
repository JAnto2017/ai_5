# METAHEURÍSTICOS

> Las siguientes secciones presentan los principales algoritmos: **algoritmo voraz**, **descenso por gradiente**, **búsqueda tabú**, **recocido simulado** y **optimización por enjambre de partículas**.

## Formulación de problemas

> Todos los problemas de optimización pueden expresarse de la misma manera. Existe una función **f** que asocia un valor con una solución **x**, denominada **f(x)**.
> Existe, sin embargo, restricciones sobre **x**, y las soluciones deben pertenecer al conjunto **X** de las soluciones aceptables.
> La optimización consiste en encontrar **x** de cara a minimizar o a maximizar **f(x)**.
> En la práctica, nos interesamos únicamente en los mínimos. Buscar maximizar **f(x)** equivale a minimizar **-f(x)**. Los algoritmos se presentarán únicamente para minimizaciones.

## Resolución matemática

La primera solución consiste en estudiar la función **f** matemáticamente y encontrar el mínimo global de la función.

Cada solución puede escribirse como un vector de dimensión **N**, siendo *N* el número de objetos posibles, con cada valor que vale *0* ó *1*. 

El peso de un objeto será **p** y su valor **v**. La derivada de la función permite el estudio clásico, aunque encontrar un óptico matemático no es la solución.

## Búsqueda

La segunda solución, tras la matemática, es la solución por búsqueda exhaustiva. Comprobando todas las posibilidades, necesariamente se encontrará la solución óptima.

La búsqueda exhaustiva, en el mejor de los casos, demasiado larga para ejecutarse. En el peor de los casos, es imposible su solución.

## Mataheurísticos

Existe una familia de métodos llamados **metaheurísticos** peseendo éstos, diferentes características:
* Son genéricos y adaptables a un gran número de problemas.
* Son interactivos, tratando de mejorar resultados conforme avanzan.
* Son estocásticos, utilizan una parte más o menos importantes de azar.
* No garantizan encontrar el óptimo global.

# Algoritmos voraces

Los **algoritmos voraces** son los más sencillos.

Construyen más que una única solución, pero de manera iterativa. Así, conforme avanza el tiempo, se agrega un elemento, el más prometedor.

Este algoritmo debe adaptarse a cada problema, manteniendo únicamente el principio general.

# Descenso por gradiente

El **descenso por gradiente** es un metaheurístico incremental.

A partir de una primera solución, escogida aleatoriamente o definida como base de partida, el algoritmo buscará una optimización sin modificar la solución más que en una unidad.

Cuando el problema es una función matemática, se calcula la derivada en el punto que representa la solución actual, y se sigue la dirección de la derivada más fuerte negativa.

Por lo general, la derivada matemática no es accesible. No es posible, por lo tanto, seguirla directamente. En su lugar, se calcularán las soluciones vecinas, a una distancia de una unidad. A continuación, se evalúa cada solución. Si se encuentra una solución mejor, entonces se parte de nuevo desde esta solución para una nueva iteración. En ausencia de mejora, nos detendremos.

Los defectos son:

+ Algoritmo lento, al buscar todas las soluciones vecinas y evaluarlas.
+ El algoritmo no encuentra más que un óptimo local.
+ Se bloquea en el primer óptimo encontrado.
+ Incluso aunque la solución de partida se encuentre en el vecindario adecuada, si algún óptimo local tiene una derivada mas fuerte que el óptimo global, va a atraer al algoritmo.

# Búsqueda tabú

La **búsqueda tabú** es una mejora de la búsqueda mediante descenso por gradiente.
El algoritmo se *pasea* por el espacio de la solución y no se detiene en el primer óptimo descubierto. Se detendrá cuando todos los vecinos hayan sido visitados, tras un número de iteraciones máximo prefijado o cuando no se detecte ninguna mejora sustancial tras *x* pasos.

Esta lista se implementa, a menudo, como una lista *FIFO*.

Es posible integrar otros dos procesos a la búsqueda tabú: *la intensificación* y *la diversificación*.

La **intensificación** consiste en favorecer ciertas zonas del espacio que parecen más prometedoras.

La **diversificación** tiene como objetivo favorecer el hallazgo de nuevas zonas del espacio de búsqueda. De esta forma, se almacenan las posiciones probadas hasta el momento y se favorecen las soluciones diferentes. De este modo, es posible encontrar nuevas soluciones óptimas.

# Recocido Simulado

El **recocido simulado** mejora el descenso por gradiente y se inspira en el recocido utilizando en metalurgia.

La **regla de Metropolis**, define la probabilidad de aceptar, una solución menos óptima que la actual: ***P = e ^ (-E/T)***. Donde *E* representa la pérdida de calidad de la solución. *T* es la temperatura actual del sistema. Esta exponencial está siempre comprendida entre 0 y 1.

# Optimización por enjambre de partículas

La **optimización por enjambre de partículas** se inspira en la biología.

En este algoritmo, varias soluciones potenciales cohabitan en el espacio de búsqueda, y cada una se desplaza en una dirección determinada. Con cada iteración, las soluciones se van a desplazar como en una nube, evanzando hacia zonas que parezcan más interesantes.

Cada solución debe conocer su velocidad actual, según un vector y las mejores posiciones descubiertas hasta el momento. Además, todas las soluciones del enjambre conocen la mejor solución actual y su ubicación.

# Resultados Obtenidos

> La salida obtenida, mostrando el valor y el peso de la mochila más su contenido.
> Todas las soluciones son idénticas y se corresponden con el óptimo para la primera mochila.
> Para el problema aleatorio, dos algoritmos devuelven el mejor resultado: *el descenso por gradiente* y la *búsqueda tabú*.

![Metahurísticos](ai_5_1.png)

![Metahurísticos de optimizacion](ai_5_2.png)