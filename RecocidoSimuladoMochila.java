package AI_5;

/**
 * Se codifican los detalles de la implementación adaptadas al problema.
 * Se necesitan, dos indicadores para detener el algorirmo: nº de iteraciones desde el comienzo y el nº de iteraciones sin encontrar una mejora.
 * 
 */
public class RecocidoSimuladoMochila extends RecocidoSimulado {
    protected int numIteracionesSinMejora = 0;
    protected int numIteraciones = 0;
    private final static int NUM_MAX_ITERACIONES = 100;
    private final static int NUM_MAX_ITERACIONES_SIN_MEJORA = 30;

    /**
     * Actualiza la temperatura en cada iteración.
     * Se aplica multiplicador 0.95, que permite disminuirla de forma gradual.
     */
    @Override
    protected void ActualizarTemperatura() {
        temperatura *= 0.95;
    }

    /**
     * Se parte de 5.
     */
    @Override
    protected void InicializarTemperatura() {
        temperatura = 5;
    }

    /**
     * Permite detener el algoritmo.
     * Se comprueba entonces que ambos contadores sean inferiores a los valores máximos.
     */
    @Override
    protected boolean CriterioParada() {
        
        return numIteraciones > NUM_MAX_ITERACIONES || numIteracionesSinMejora > NUM_MAX_ITERACIONES_SIN_MEJORA;
    }

    /**
     * Permite saber si es necesario o no actualizar la solución actual.
     * Consulta una solución que entrañe una pérdida de calidad.
     * En caso afirmativo, calcula probabilidad de aceptarla gracias a la ley de Metropolis.
     * Extrae un nº aleatorio inferior a esta probabilidad, se actualiza la solución actual.
     * Si es la mejor encontrada hasta el momento, se actualiza la variable mejorSolucion y se reinicia el nº de iteraciones sin mejora.
     * 
     */
    @Override
    protected void Actualizar(ISolucion solucion) {
        double probabilidad = 0.0;
        if (solucion.getValor() < solucionActual.getValor()) {
            probabilidad = Math.exp(-1 * (solucionActual.getValor() - solucion.getValor()) / solucionActual.getValor() / temperatura);
        }

        if (solucion.getValor() > solucionActual.getValor() || ProblemaMochila.generador.nextDouble() < probabilidad) {
            solucionActual = solucion;
            if (solucion.getValor() > mejorSolucion.getValor()) {
                mejorSolucion = solucion;
                numIteracionesSinMejora = 0;
            }
        }
    }

    /**
     * 
     */
    @Override
    protected void Incrementar() {
        numIteracionesSinMejora++;
        numIteraciones++;
    }

    /**
     * Ejecuta la visualización de la mejor solución encontrada hasta el momento.
     * 
     */
    @Override
    protected void DevolverResultado() {
        ihm.MostrarMensaje(mejorSolucion.toString());
    }
}
