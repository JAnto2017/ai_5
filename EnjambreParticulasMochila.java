package AI_5;

import java.util.ArrayList;

/**
 * La clase EjembreParticulas contiene cinco métodos abstractos que hay que implementar.
 * 
 * En lugar de escoger la mejor solución de un vecindario, debemos hacer evolucionar todas nuestras soluciones con el paso del tiempo en función de la mejor solución encontrada hasta el momento y de la mejor solución en curso.
 * 
 */
public class EnjambreParticulasMochila extends EnjambreParticulas {
    protected int numIteraciones = 0;
    private final static int NUM_MAX_ITERACIONES = 200;

    /**
     * 
     */
    @Override
    protected void ActualizarSoluciones() {
        for (ISolucion sol : soluciones) {
            SolucionMochila solucion = (SolucionMochila) sol;

            if (!solucion.equals(mejorSolucion)) {
                solucion = AgregarElemento(solucion, mejorSolucion);
                solucion = AgregarElemento(solucion, mejorActual);

                //disminución del peso si hace falta
                int indice;

                while (solucion.getPeso() > ((ProblemaMochila) problema).pesoMax) {
                    indice = ProblemaMochila.generador.nextInt(solucion.contenido.size());
                    solucion.contenido.remove(indice);
                }
                solucion = Completar(solucion);
            }
        }
    }

    /**
     * 
     * @param Solucion
     * @param SolucionOrigen
     * @return
     */
    protected SolucionMochila AgregarElemento(SolucionMochila Solucion, ISolucion SolucionOrigen) {
        int indice = ProblemaMochila.generador.nextInt(((SolucionMochila) SolucionOrigen).contenido.size());

        Caja b = ((SolucionMochila) SolucionOrigen).contenido.get(indice);

        if (!Solucion.contenido.contains(b)) {
            Solucion.contenido.add(b);
        }

        return Solucion;
    }

    /**
     * 
     * @param Solucion
     * @return
     */
    protected SolucionMochila Completar(SolucionMochila Solucion) {
        double espacioDispo = ((ProblemaMochila) problema).pesoMax - Solucion.getPeso();
        
        ArrayList<Caja> cajasPosibles = ((ProblemaMochila) problema).Cajas();

        cajasPosibles.removeAll(Solucion.contenido);

        ((ProblemaMochila)problema).EliminarDemasiadoPesadas(cajasPosibles, espacioDispo);

        Caja b;
        
        int indice;

        while (!cajasPosibles.isEmpty()) {
            indice = ProblemaMochila.generador.nextInt(cajasPosibles.size());
            b = cajasPosibles.get(indice);
            Solucion.contenido.add(b);
            cajasPosibles.remove(b);
            espacioDispo = ((ProblemaMochila)problema).pesoMax - Solucion.getPeso();
            ((ProblemaMochila)problema).EliminarDemasiadoPesadas(cajasPosibles, espacioDispo);
        }
        return Solucion;
    }

    /**
     * Debe actualizar las mejores soluciones.
     * La mejor de la población en curso y potencialmente la mejor encontrada desde el inicio.
     * Se recorre la población actual y potencialmente modificar mejorSolucion.
     * 
     */
    @Override
    protected void ActualizarVariables() {
        mejorActual = soluciones.get(0);
        for (ISolucion sol : soluciones) {
            if (sol.getValor() > mejorActual.getValor()) {
                mejorActual = sol;
            }
        }

        if (mejorActual.getValor() > mejorSolucion.getValor()) {
            mejorSolucion = mejorActual;
        }
    }

    /**
     * Comprueba que no se ha alcanzado el criterio de parada.
     * Basta con verificar el nº de iteraciones.
     * 
     */
    @Override
    protected boolean CriterioParada() {        
        return numIteraciones > NUM_MAX_ITERACIONES;
    }

    /**
     * Incrementa simplemente el nº de iteraciones.
     * 
     */
    @Override
    protected void Incrementar() {
        numIteraciones++;
    }

    /**
     * Muestra la mejor solución encontrada hasta el momento.
     * 
     */
    @Override
    protected void DevolverResultado() {
        ihm.MostrarMensaje(mejorSolucion.toString());
    }
}
