package AI_5;

import java.util.ArrayList;

import org.hamcrest.core.Is;

/**
 * Inicializa la solución de partida aleatoriamente.
 * Mientras no se alcancen los criterios de parada, se crea el vecindario de la solución actual.
 * Las soluciones presentes en la lista de posiciones tabús se eliminan.
 * Se guarda la mejor solución con su actualización.
 */
public abstract class BusquedaTabu extends Algoritmo {
    protected ISolucion solucionActual;
    protected ISolucion mejorSolucion;

    @Override
    public void Resolver(IProblema _pb, IHM _ihm) {
        
        super.Resolver(_pb, _ihm);

        solucionActual = problema.SolucionAleatoria();
        mejorSolucion = solucionActual;
        AgregarListaTabu(solucionActual);

        while (!CriterioParada()) {
            ArrayList<ISolucion> vecindario = problema.Vecindario(solucionActual);

            if (vecindario != null) {
                vecindario = EliminarSolucionTabus(vecindario);
                ISolucion mejorVecino = problema.MejorSolucion(vecindario);

                if (mejorVecino != null) {
                    Actualizar(mejorVecino);
                }
            }
            Incrementar();
        }
        DevolverResultado();
    }

    protected abstract ArrayList<ISolucion> EliminarSolucionTabus(ArrayList<ISolucion> vecindario);

    protected abstract void AgregarListaTabu(ISolucion solucion);
    //protected abstract ArrayList<ISolucion> EliminarSolucionTabus(ArrayList<ISolucion> vecindario);

    protected abstract boolean CriterioParada();
    protected abstract void Actualizar(ISolucion solucion);
    protected abstract void Incrementar();
}
