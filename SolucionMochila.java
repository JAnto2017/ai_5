package AI_5;

import java.util.ArrayList;
import java.util.StringJoiner;

public class SolucionMochila implements ISolucion {
    public ArrayList<Caja> contenido;

    public  SolucionMochila() {
        contenido = new ArrayList<>();
    }

    /**
     * Constructor que permite inicializar esta lista y otro que copia el contenido el contenido de una solución que se pasa como parámetro.
     * @param original
     */
    public SolucionMochila(SolucionMochila original) {
        contenido = new ArrayList<>();
        contenido.addAll(original.contenido);
    }

    /**
     * Calcula el peso de una solución, sumando los pesos de cada caja.
     * @return
     */
    public double getPeso() {
        double peso = 0.0;
        for (Caja b : contenido) {
            peso += b.peso;
        }
        return peso;
    }

    /**
     * calcula el valor total de una solución, caja por caja
     */
    @Override
    public double getValor() {
        double valor = 0.0;
        for (Caja b : contenido) {
            valor += b.valor;
        }
        return valor;
    }

    /**
     * Crea una cadena con el valor, peso y contenido de la mochila.
     */
    @Override
    public String toString() {
        StringJoiner sj = new StringJoiner(" - ");
        sj.add("Valor: " + getValor());
        sj.add("Peso: " + getPeso());
        for (Caja b : contenido) {
            sj.add(b.toString());
        }
        return sj.toString();
    }

    /**
     * Compara primero si ambas mochilas contienen el mismo número de objetos, el mismo peso y el mismo valor.
     * Si se da el caso, se comprueba entonces si cada caja contenida en la primera mochila se encuentra en la segunda.
     * No es posible comparar directametne las listas, puesto que los objetos pueden encontrarse en un orden diferente.
     * 
     */
    @Override
    public boolean equals(Object obj) {
        if (!(obj instanceof SolucionMochila)) {
            return false;
        }

        SolucionMochila sol = (SolucionMochila) obj;

        if (sol.contenido.size() != this.contenido.size() || sol.getPeso() != this.getPeso() || sol.getValor() != this.getValor()) {
            return false;
        }

        for (Caja b : contenido) {
            if (!sol.contenido.contains(b)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Diferencia rápidamente las soluciones si se utilizan en un diccionario.
     * Desafortunadamente ninguno de los atributos actuales es fijo. Se devuelve el mismo valor 42.
     */
    @Override
    public int hashCode() {
        return 42;
    }
}
