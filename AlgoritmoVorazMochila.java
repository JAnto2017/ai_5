package AI_5;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Tiene dos métodos.
 * Uno permite construir la solución.
 * Otro permite mostrar el resultado.
 * 
 */
public class AlgoritmoVorazMochila extends AlgoritmoVoraz {
    SolucionMochila solucion;

    /**
     * Pide la lista de cajas que pueden utilizarse.
     * Se las ordena, de mayor a menor según la relación valor/peso.
     * Mientras quede espacio en la mochila, se agregarn cajas en orden.
     * Para ordenar, se utiliza una expresión lambda que permite indicar qué caja es mejor que otra.
     */
    @Override
    protected void ConstruirSolucion() {
        solucion = new SolucionMochila();
        ProblemaMochila pb = (ProblemaMochila) problema;
        ArrayList<Caja> cajasPosibles = pb.Cajas();

        Collections.sort(cajasPosibles, (Caja b1, Caja b2) -> (int) (((b2.valor/b2.peso) >= (b1.valor/b1.peso)) ? 1 : -1));

        double espacioDispo = pb.pesoMax;

        for (Caja b : cajasPosibles) {
            if (b.peso <= espacioDispo) {
                solucion.contenido.add(b);
                espacioDispo -= b.peso;
            }
        }        
    }

    /**
     * 
     */
    @Override
    protected void DevolverResultado() {
        ihm.MostrarMensaje(solucion.toString());
    }
}
