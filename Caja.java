package AI_5;

/**
 * Crear la cajas con constructor.
 * Actualiza tres constructores.
 * toString para mostrar la info.
 * 
 */
public class Caja {
    public double peso;
    public double valor;
    protected String nombre;

    public Caja(String _nombre, double _peso, double _valor) {
        nombre = _nombre;
        peso = _peso;
        valor = _valor;        
    }

    @Override
    public String toString() {
        return nombre + " (" + peso + ", " + valor + ")";
    }
}
