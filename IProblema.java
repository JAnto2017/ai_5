package AI_5;

import java.util.ArrayList;

import org.hamcrest.core.Is;

/**
 * Solución aleatoria.
 * El vecindario de una solución.
 * La mejor solución de entre una lista pasada como parámetro.
 */
public interface IProblema {
    ArrayList<ISolucion> Vecindario(ISolucion solucionActual);
    ISolucion SolucionAleatoria();
    ISolucion MejorSolucion(ArrayList<ISolucion> soluciones);
}
