package AI_5;

import javax.naming.spi.Resolver;

/**
 * Un método pide resolver un problema y el otro, permite mostrar el resultado del algoritmo.
 * Dos atributos: uno permite tener un enlace hacia el problema que se ha de resolver y otro hacia la clase que sirve como interfaz con el usuario.
 */
public abstract class Algoritmo {
    protected IProblema problema;
    protected IHM ihm;

    public void Resolver(IProblema _pb, IHM _ihm) {
        problema = _pb;
        ihm = _ihm;
    }

    protected abstract void DevolverResultado();
}
