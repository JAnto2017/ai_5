package AI_5;

import java.util.ArrayList;

import org.hamcrest.core.Is;


/**
 * Crea una primera solución aleatoria y a continuación,
 * mientras no se alcance un criterio de parada, se pide el
 * vecindario de una solución;
 * Si el vecindario existe, se recoge la mejor solución en su interior.
 * Se realiza entonces la actualización de la solución en curso mediante Actualizar.
 * El bucle termina incrementando las distintas variables que permiten alcanzar el criterio de parada.
 */
public abstract class DescensoGradiente extends Algoritmo{
    protected ISolucion solucionActual;

    @Override
    public void Resolver(IProblema _pb, IHM _ihm) {
        
        super.Resolver(_pb, _ihm);

        solucionActual = problema.SolucionAleatoria();

        while (!CriterioParada()) {
            ArrayList<ISolucion> vecindario = problema.Vecindario(solucionActual);

            if (vecindario != null) {
                ISolucion mejorSolucion = problema.MejorSolucion(vecindario);
                Actualizar(mejorSolucion);
            }
            Incrementar();
        }
        DevolverResultado();
    }

    protected abstract boolean CriterioParada();
    protected abstract void Actualizar(ISolucion solucion);
    protected abstract void Incrementar();
}
