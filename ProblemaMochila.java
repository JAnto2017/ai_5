package AI_5;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import org.hamcrest.core.Is;

/**
 * Contiene una lista de cajas disponibles para incluir en las mochilas 'cajasDispo'
 * así como un peso máximo admitido.
 * Un generador aleatorio que se define estático y una constante que indica el número de vecinos
 * que se creará para cada solución.
 * Este nº puede modificarse en función de las necesidades.
 * 
 */
public class ProblemaMochila implements IProblema {
    protected ArrayList<Caja> cajasDispo = null;
    public double pesoMax;
    public static Random generador = null;
    public final static int  NUM_VECINOS = 30;

    /**
     * Constructor I
     * 
     */
    public ProblemaMochila() {
        //lista de cajas
        cajasDispo = new ArrayList<>();
        
        cajasDispo.add(new Caja("A", 4, 15));
        cajasDispo.add(new Caja("B", 7, 15));
        cajasDispo.add(new Caja("C", 10, 20));
        cajasDispo.add(new Caja("D", 3, 10));
        cajasDispo.add(new Caja("E", 6, 11));
        cajasDispo.add(new Caja("F", 12, 16));
        cajasDispo.add(new Caja("G", 11, 12));
        cajasDispo.add(new Caja("H", 16, 22));
        cajasDispo.add(new Caja("I", 5, 12));
        cajasDispo.add(new Caja("J", 14, 21));
        cajasDispo.add(new Caja("K", 14, 21));
        cajasDispo.add(new Caja("L", 3, 7));

        pesoMax = 20;

        if (generador == null) {
            generador = new Random();
        }
    }

    /**
     * Constructor II
     * 
     * @param numCajas
     * @param _pesoMax
     * @param valorMax
     */
    public ProblemaMochila(int numCajas, double _pesoMax, double valorMax) {
        cajasDispo = new ArrayList<>();
        pesoMax = _pesoMax;

        if (generador == null) {
            generador = new Random();
        }

        for (int i = 0; i < numCajas; i++) {
            cajasDispo.add(new Caja(Integer.toString(i), generador.nextDouble() * pesoMax, generador.nextDouble() * valorMax));
        }
    }

    /**
     * Devuelve una copia de la lista de cajas disponibles.
     * 
     * @return
     */
    public ArrayList<Caja> Cajas() {
        ArrayList<Caja> copia = new ArrayList<>();
        copia.addAll(cajasDispo);
        return copia;
    }

    /**
     * Crea aleatoriamente una nueva solución.
     * Se echan a suertes las cajas una a una.
     * 
     */
    @Override
    public ISolucion SolucionAleatoria() {
        SolucionMochila solucion = new SolucionMochila();
        ArrayList<Caja> cajasPosibles = Cajas();
        double espacioDispo = pesoMax;
        EliminarDemasiadoPesadas(cajasPosibles, espacioDispo);

        while (espacioDispo > 0 && !cajasPosibles.isEmpty()) {
            int indice = generador.nextInt(cajasPosibles.size());
            Caja b = cajasPosibles.get(indice);
            solucion.contenido.add(b);
            cajasPosibles.remove(indice);
            espacioDispo -= b.peso;
            EliminarDemasiadoPesadas(cajasPosibles, espacioDispo);
        }
        return solucion;
    }

    /**
     * Cuando no pueden agregarse más cajas a la mochila, se devuelve la solucion creada.
     * No seleccionamos cajas ya escogidas ni demasiado pesadas.
     * 
     * @param cajasPosibles
     * @param espacioDispo
     */
    public void EliminarDemasiadoPesadas(ArrayList<Caja> cajasPosibles, double espacioDispo) {
        Iterator<Caja> iterador = cajasPosibles.iterator();

        while (iterador.hasNext()) {
            Caja b = iterador.next();

            if (b.peso > espacioDispo) {
                iterador.remove();
            }
        }
    }

    /**
     * Escoge la mejor solución de una lista.
     * Busca la solución con el valor máximo. 
     * Para ello, se recorre la lista y se conserva la mejor solución conforme se avanza.
     */
    @Override
    public ISolucion MejorSolucion(ArrayList<ISolucion> soluciones) {
        if (!soluciones.isEmpty()) {
            ISolucion mejor = soluciones.get(0);
            for (ISolucion sol : soluciones) {
                if (sol.getValor() > mejor.getValor()) {
                    mejor = sol;
                }
            }
            return mejor;
        } else {
            return null;
        }
    }

    /**
     * Consiste en devolver el vecindario de la solución que se pasa como parámetro.
     * Se elimina un objeto al azar y se completa el espacio liberado por otras cajas, aleatoriamente.
     * Se repite tantas veces como vecinos se desee tener.
     */
    @Override
    public ArrayList<ISolucion> Vecindario(ISolucion solucionActual) {
        ArrayList<ISolucion> vecindario = new ArrayList<>();

        for (int i = 0; i < NUM_VECINOS; i++) {
            SolucionMochila solucion = new SolucionMochila((SolucionMochila) solucionActual);

            int indice = generador.nextInt(solucion.contenido.size());
            solucion.contenido.remove(indice);

            ArrayList<Caja> cajasPosibles = Cajas();

            double espacioDispo = pesoMax - solucion.getPeso();

            cajasPosibles.removeAll(solucion.contenido);

            EliminarDemasiadoPesadas(cajasPosibles, espacioDispo);

            while (espacioDispo > 0 && !cajasPosibles.isEmpty()) {
                indice = generador.nextInt(cajasPosibles.size());
                Caja b = cajasPosibles.get(indice);
                solucion.contenido.add(b);
                cajasPosibles.remove(indice);
                espacioDispo -= b.peso;
                EliminarDemasiadoPesadas(cajasPosibles, espacioDispo);
            }
            vecindario.add(solucion);
        }
        return vecindario;
    }
}
