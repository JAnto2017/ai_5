package AI_5;

public class Mochila implements IHM {
    @Override
    public void MostrarMensaje(String msg) {
        System.out.println(msg);
    }

    //------------------------------------------------------
    /**
     * 
     * @param pb
     */
    private void EjecutarAlgoritmos(IProblema pb) {
        Algoritmo algo;

        System.out.println("Algoritmo voraz");
        algo = new AlgoritmoVorazMochila();
        algo.Resolver(pb, this);
        System.out.println();

        System.out.println("Descenso por gradiente");
        algo = new DescensoGradienteMochila();
        algo.Resolver(pb, this);
        System.out.println();

        System.out.println("Búsqueda tabú");
        algo = new BusquedaTabuMochila();
        algo.Resolver(pb, this);
        System.out.println();

        System.out.println("Recocido simulado");
        algo = new RecocidoSimuladoMochila();
        algo.Resolver(pb, this);
        System.out.println();

        System.out.println("Ejambre de partículas");
        algo = new EnjambreParticulasMochila();
        algo.Resolver(pb, this);
        System.out.println();
    }

    /**
     * Permite ejecutar los distintos algoritmos sobre el problema de la mochila simple.
     * Con 100 cajas de un valor máximo de 20, para una mochila que puede contener hasta 30 kg.
     * 
     */
    private void Ejecutar() {
        System.out.println("Metaheurísticos de optimización");
        ProblemaMochila pb = new ProblemaMochila();
        EjecutarAlgoritmos(pb);
        System.out.println("***************************************\n");
        pb = new ProblemaMochila(100, 30, 20);
        EjecutarAlgoritmos(pb);
    }

    /**
     * **********************************************************************
     * **********************************************************************
     * @param args
     */
    public static void main(String[] args) {
        Mochila app = new Mochila();
        app.Ejecutar();
    }
}
