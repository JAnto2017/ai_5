package AI_5;

import java.util.ArrayList;

/**
 * En lugar de utilizar un vecindario compuesto de varias soluciones y mantener una sola, se tiene una población de soluciones que se desplazarán en el entorno.
 * Además de la mejor solución encontrada hasta el momento, se guardará también la mejor solución en el seno d ela población.
 * La constante NUM_IND. indica el número de individuos utilizados en la población. Es un parámetro a configurar.
 * 
 */
public abstract class EnjambreParticulas extends Algoritmo {
    protected ArrayList<ISolucion> soluciones;
    protected ISolucion mejorSolucion;
    protected ISolucion mejorActual;

    protected final static int NUM_INDIVIDUOS = 30;

    @Override
    public void Resolver(IProblema _pb, IHM _ihm) {
        
        super.Resolver(_pb, _ihm);

        soluciones = new ArrayList<>();

        for (int i = 0; i < NUM_INDIVIDUOS; i++) {
            ISolucion nuevaSol = problema.SolucionAleatoria();
            soluciones.add(nuevaSol);
        }

        mejorSolucion = problema.MejorSolucion(soluciones);
        mejorActual = mejorSolucion;

        while (!CriterioParada()) {
            ActualizarVariables();
            ActualizarSoluciones();
            Incrementar();
        }

        DevolverResultado();
    }

    protected abstract void ActualizarVariables();
    protected abstract void ActualizarSoluciones();  
    protected abstract boolean CriterioParada();
    protected abstract void Incrementar();
}
